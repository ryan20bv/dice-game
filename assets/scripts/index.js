const btn = document.querySelector('.btn');
const imgLeft = document.querySelector('#playerOne img');
const imgRight = document.querySelector('#playerTwo img');
const scoreLeft = document.querySelector('#playerOne .score');
const scoreRight = document.querySelector('#playerTwo .score');
const leftMessage = document.querySelector('#playerOne').lastElementChild;
const rightMessage = document.querySelector('#playerTwo').lastElementChild;

let randomNum1 = 0;
let randomNum2 = 0;
let playerOneScore = 0;
let playerTwoScore = 0;

function buttonClick() {
	randomNum1 = Math.ceil(Math.random() * 6);
	randomNum2 = Math.ceil(Math.random() * 6);

	imgLeft.setAttribute('src', `assets/images/dice${randomNum1}.png`);
	imgRight.setAttribute('src', `assets/images/dice${randomNum2}.png`);
}
function updateScore() {
	if (randomNum1 > randomNum2) {
		playerOneScore++;
		scoreLeft.innerHTML = playerOneScore;
		showMessageLeft();
	} else if (randomNum2 > randomNum1) {
		playerTwoScore++;
		scoreRight.innerHTML = playerTwoScore;
		showMessageRight();
	} else {
		drawMessage();
		showMessageLeft();
		showMessageRight();
	}
}

function showMessageLeft() {
	leftMessage.classList.add('visibility');
}
function showMessageRight() {
	rightMessage.classList.add('visibility');
}
function resetMessage() {
	leftMessage.innerHTML = 'Win';
	leftMessage.classList.remove('visibility');
	rightMessage.innerHTML = 'Win';
	rightMessage.classList.remove('visibility');
}
function drawMessage() {
	leftMessage.innerHTML = 'Draw';
	rightMessage.innerHTML = 'Draw';
}

function automaticRandom() {
	resetMessage();
	let autoId = setInterval(buttonClick, 500);
	setTimeout(() => stopAutomatic(autoId), 5000);
}
function stopAutomatic(id) {
	clearInterval(id);
	updateScore();
}

btn.addEventListener('click', automaticRandom);
